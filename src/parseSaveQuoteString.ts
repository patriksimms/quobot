export function parseSaveQuoteString(command: string) {
  const pattern = /^\s*["“”](.*)["“”]\s*(.*)\s*$/
  const matches = command.match(pattern)

  if (matches && matches.length === 3) {
    return {
      quote: matches[1].trim(),
      author: matches[2].trim()
    }
  }
  return null
}
