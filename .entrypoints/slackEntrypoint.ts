import express from 'express';

import { Logger } from '../src/Logger';
import { Connector } from '../src/api/Connector';
import { SlackConnector } from '../src/api/SlackConnector';
import { PersistanceClient } from '../src/persistence/PersistanceClient.interface';
import { SupabasePersistanceClient } from '../src/persistence/SupabasePersistanceClient';
import { Bot } from '../src/Bot';
(async () => {
  const logger = new Logger()
  const server = express()

  server.get('/', (req, res, next) => {
    res.json({ msg: 'ok' })
  })

  server.use('/imgCache', express.static('./imgCache'))

  server.listen(80, () => {
    logger.info('HTTP server is listening...')
  })

  const persistanceClient: PersistanceClient = new SupabasePersistanceClient(logger)

  const bot = new Bot({connector: SlackConnector, persistanceClient: persistanceClient}, logger)

  await bot.start(process.env.ALLOW_RANDOM_QUOTE_FETCHING === 'true')
  await bot.startPostQuoteCron(process.env.QUOTE_POST_AT, process.env.QUOTE_SEND_CHANNEL)

})()


