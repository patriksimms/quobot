import { Quote } from "../types";

export interface PersistanceClient {

  saveQuote(quote: Quote): Promise<void>

  getQuoteById(quoteId: number): Promise<Quote>

  getRandomQuote(): Promise<Quote>
}
