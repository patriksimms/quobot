
export interface Connector {

  registerExternalClient(): Promise<void>

  registerQuoteSubmitMethod(): void

  registerRandomQuoteFetchingMethod(allowRandomQuoteFetching: boolean): void

  postImage(channelId: string, imgPath: string): Promise<void>
}
