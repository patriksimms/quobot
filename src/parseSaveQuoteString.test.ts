import { describe, expect, test } from "vitest"
import { parseSaveQuoteString } from "./parseSaveQuoteString"

describe('SlackConnector', () => {

  test('parseQuoteString works with correct', () => {
    const command = '"Some Test" someAuthor'
    expect(parseSaveQuoteString(command)?.quote).toBe('Some Test')
    expect(parseSaveQuoteString(command)?.author).toBe('someAuthor')
  })

  test('parseQuoteString works with whitespace on start & end', () => {
    const command = ' "Some Test" someAuthor '
    expect(parseSaveQuoteString(command)?.quote).toBe('Some Test')
    expect(parseSaveQuoteString(command)?.author).toBe('someAuthor')
  })

  test('parseQuoteString works with more whitespace between quote & author', () => {
    const command = '"Some Test"      someAuthor'
    expect(parseSaveQuoteString(command)?.quote).toBe('Some Test')
    expect(parseSaveQuoteString(command)?.author).toBe('someAuthor')
  })

  test('parseQuoteString works without whitespace between quote & author', () => {
    const command = '"Some Test"someAuthor'
    expect(parseSaveQuoteString(command)?.quote).toBe('Some Test')
    expect(parseSaveQuoteString(command)?.author).toBe('someAuthor')
  })

})
