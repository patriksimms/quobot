import { SupabaseClient, createClient } from '@supabase/supabase-js'
import { Quote } from '../types'
import { PersistanceClient } from './PersistanceClient.interface'
import { Database } from '../../supabase'
import { Logger } from '../Logger'
import { randomUUID } from 'crypto'

// https://community.weweb.io/t/query-random-item-s-from-database-supabase/1159
export class SupabasePersistanceClient implements PersistanceClient {
  private static SUPABASE_URL = process.env.SUPBABASE_URL ?? ''

  private readonly client: SupabaseClient<Database>
  private readonly logger: Logger

  constructor(logger: Logger) {
    this.client = createClient<Database>(SupabasePersistanceClient.SUPABASE_URL, process.env.SUPABASE_ACCESS_KEY!)
    this.logger = logger
  }

  async saveQuote(quote: Quote): Promise<void> {
    const { error, status } = await this.client.from('Quotes').insert([
      {
        author: quote.author,
        linkToOrigin: quote.linkToOrigin,
        quote: quote.quote,
        timestamp: '' + new Date().toISOString(),
        id: quote.id ?? randomUUID()
      }
    ])
    if (error) {
      this.logger.error(`${error.message}. Hint: ${error.hint}`)
      throw error
    }
  }

  async getQuoteById(quoteId: number): Promise<Quote> {
    const { data, error } = await this.client.from('Quotes').select('*').eq('id', quoteId)

    if (error) {
      throw error
    }

    return {
      author: data[0].author,
      linkToOrigin: data[0].linkToOrigin ?? '',
      quote: data[0].quote,
      timestamp: new Date(data[0].timestamp!),
      id: data[0].id
    } as Quote
  }

  async getRandomQuote(): Promise<Quote> {
    const { data, error } = await this.client.rpc('get_random_quote')

    if (error) {
      throw error
    }

    return {
      author: data[0].author,
      linkToOrigin: data[0].linkToOrigin ?? '',
      quote: data[0].quote,
      timestamp: new Date(data[0].timestamp!),
      id: data[0].id
    } as Quote
  }
}
