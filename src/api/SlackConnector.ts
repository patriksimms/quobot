import { App } from '@slack/bolt'
import { randomUUID } from 'crypto'
import { Logger } from '../Logger'
import { QuoteHandler } from '../QuoteHandler'
import { BotInitalized } from '../decorators/BotInitalized'
import { parseSaveQuoteString } from '../parseSaveQuoteString'
import { TextProvider } from '../textProvider/TextProvider'
import { type Connector } from './Connector'

export class SlackConnector implements Connector {
  private readonly logger: Logger
  private readonly textProvider: TextProvider
  private readonly quoteHandler: QuoteHandler

  private bot!: App

  constructor(logger: Logger, textProvider: TextProvider, quoteHandler: QuoteHandler) {
    this.logger = logger
    this.textProvider = textProvider
    this.quoteHandler = quoteHandler
  }

  async registerExternalClient(): Promise<void> {
    this.bot = new App({
      signingSecret: process.env.SLACK_SIGNING_SECRET ?? '',
      token: process.env.SLACK_BOT_TOKEN ?? '',
      appToken: process.env.SLACK_APP_TOKEN ?? '',
      socketMode: true,
      port: 80
    })

    await this.bot.start()
    this.logger.info('Slack: Registered external client')
  }

  @BotInitalized()
  registerQuoteSubmitMethod(): void {

    this.bot.command('/savequote', async ({ command, ack, logger, say }) => {
      await ack()

      const matches = parseSaveQuoteString(command.text)

      if (matches !== null) {

        const newQuoteId = randomUUID()
        const { error, savedQuoteId } = await this.quoteHandler.saveQuote({
          author: matches.author,
          linkToOrigin: null,
          quote: matches.quote,
          id: newQuoteId
        })

        if (error !== null || savedQuoteId === null) {
          say({
            text: this.textProvider.unknownErrorMessage()
          })
          throw error
        }

        say({ text: this.textProvider.quoteSavedMessage(savedQuoteId) })
      } else {

        this.logger.info(`Slack: Slash Command text did not match pattern : ${command.text}`)
        say({ text: this.textProvider.unknownCommandMessage(command.text) })
      }
    })
  }

  @BotInitalized()
  registerRandomQuoteFetchingMethod(allowRandomQuoteFetching: boolean) {

    this.bot.command('/getquote', async ({ command, ack, say }) => {
      await ack()

      if (!allowRandomQuoteFetching) {
        await say({
          text: this.textProvider.fetchRandomQuoteDisallowedMessage()
        })
        return
      }

      await say({
        text: this.textProvider.quoteAnnouncement()
      })

      const imgPath = await this.quoteHandler.createRandomQuoteImage()
      this.postImage('' + command.channel_id, imgPath)
    })
  }

  async postImage(channelId: string, imgPath: string) {
    this.logger.info(`Sending QuoteImage: ${imgPath} ...`)

    await this.bot!.client.files.uploadV2({
      channel_id: channelId,
      file_uploads: [
        {
          file: imgPath,
          // TODO check that the file path is not leaked
          filename: imgPath
        }
      ]
    })
  }
}
