import winston, { createLogger } from 'winston'

export class Logger {
  private readonly logger: winston.Logger

  constructor() {
    this.logger = createLogger({
      level: process.env.NODE_ENV === 'production' ? 'info' : 'debug',
      format: winston.format.json(),
      transports: [
        new winston.transports.Console({
          format: winston.format.simple()
        })
      ]
    })
  }

  info(message: string, meta?: Object) {
    this.logger.info(message, meta)
  }

  error(message: string, meta?: Object) {
    this.logger.error(message, meta)
  }

  debug(message: string, meta?: Object) {
    this.logger.debug(message, meta)
  }
}
