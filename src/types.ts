export interface Quote {
  quote: string
  author: string
  timestamp?: Date
  linkToOrigin: string | null

  id: string | null
}

// global configuration for the bot to behave
export interface Config {
  quoteAnnouncement: string // the announcement text which will be send before the quote
}
