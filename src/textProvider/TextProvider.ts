export interface TextProvider {
  quoteSavedMessage(id: string): string
  
  unknownErrorMessage(): string

  unknownCommandMessage(command: string): string

  fetchRandomQuoteDisallowedMessage(): string

  sendingQuoteMessage(imgPath: string): string

  quoteAnnouncement(): string
}
