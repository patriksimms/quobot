import { randomUUID } from "crypto";
import { ImageProvider } from './ImageProvider';
import { PersistanceClient } from "./persistence/PersistanceClient.interface";
import { TextProvider } from './textProvider/TextProvider';
import { Quote } from "./types";
import { Logger } from "./Logger";

export class QuoteHandler {
  private readonly persistanceClient: PersistanceClient
  private readonly textProvider: TextProvider;
  private readonly imageProvider: ImageProvider;
  private readonly logger: Logger;

  constructor(persistanceClient: PersistanceClient, textProvider: TextProvider, imageProvider: ImageProvider, logger: Logger) {
    this.persistanceClient = persistanceClient
    this.textProvider = textProvider
    this.imageProvider = imageProvider
    this.logger = logger
  }

  getCommandExplanation() {
    return [
      {
        command: 'help',
        description: 'Shows availible commands and how to use them'
      },
      {
        command: 'getquote',
        description: 'gets a random quote'
      }
    ]
  }

  async saveQuote(quote: Quote) {
    // generate the id on the client side to be able to reuse the ID without querying the db agian
    try {
      const quoteId = quote.id ?? randomUUID()
      await this.persistanceClient.saveQuote({
        author: quote.author,
        linkToOrigin: '' + quote.linkToOrigin,
        quote: quote.quote,
        id: quote.id ?? randomUUID()
      })

      return { success: true, savedQuoteId: quoteId, error: null }
    } catch (e) {
      return { success: false, savedQuoteId: null, error: e }
    }
  }

  async createRandomQuoteImage() {
    try {
      const randomQuote = await this.persistanceClient.getRandomQuote()
      const imgPath = await this.imageProvider.provideImageForQuote(randomQuote)

      return imgPath
    } catch (e: unknown) {
      if (e instanceof Error) {
        this.logger.error(e.message)
      }
      throw e
    }
  }

}
