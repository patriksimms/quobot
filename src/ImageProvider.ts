import ejs from 'ejs'
import fs, { readFileSync, writeFileSync } from 'fs'
import { resolve } from 'path'
import { webkit } from 'playwright-webkit'
import { rimrafSync } from 'rimraf'
import { type Logger } from './Logger'
import { Quote } from './types'

export class ImageProvider {
  private static MAX_TEXT_WIDTH = 1500
  private static IMG_DIR = './imgCache'
  private readonly logger: Logger

  backgroundImage = './theatre.jpg'

  constructor(logger: Logger) {
    const res = rimrafSync(ImageProvider.IMG_DIR + '/*', { glob: true })

    if (!res) {
      throw new Error(`Something wrent wrong while clrearing ${ImageProvider.IMG_DIR}!`)
    }
    this.logger = logger
    this.logger.debug(`rimraf ${ImageProvider.IMG_DIR + '/*'} ...`)
  }

  async provideImageForQuote(quote: Quote): Promise<string> {
    if (fs.existsSync(`${ImageProvider.IMG_DIR}${quote.id}.jpg`)) {
      return `${ImageProvider.IMG_DIR}${quote.id}.jpg`
    }

    const browser = await webkit.launch({ headless: true })
    const page = await browser.newPage()

    await page.setViewportSize({
      height: 600,
      width: 900
    })

    const template = readFileSync(resolve('./template.ejs'), 'utf-8')
    const backgroundImagePath = this.backgroundImage.startsWith('https://') ? this.backgroundImage : resolve(this.backgroundImage)
    const renderedHtml = ejs.render(template, {
      imagePath: backgroundImagePath,
      quote: quote.quote,
      author: `${quote.author} - ${quote.timestamp?.toLocaleDateString('de-DE') ?? 'someday'}`
    })

    await page.goto('file://' + resolve('./template.ejs'))
    await page.setContent(renderedHtml)

    await page.waitForLoadState('networkidle')

    const imageBuffer = await page.screenshot()

    await browser.close()

    const imgPath = `${ImageProvider.IMG_DIR}/${quote.id}.jpg`
    await writeFileSync(imgPath, imageBuffer)

    this.logger.debug(`created a new image ${imgPath}`)

    return imgPath
  }
}
