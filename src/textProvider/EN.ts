import { TextProvider } from './TextProvider';

export class EN implements TextProvider {

  quoteSavedMessage(id: string): string {
    return `Saved quote! ID: ${id}`
  }
  unknownErrorMessage(): string {
    return 'An unknown error occured!'
  }
  unknownCommandMessage(command: string): string {
    return `Slash Command text did not match pattern : ${command}. The bot expects it in the following format: "the quote" the author`
  }
  fetchRandomQuoteDisallowedMessage(): string {
    return 'Fetching a quote on demand is not allowed for this bot! Please change the "ALLOW_RANDOM_QUOTE_FETCHING" config'
  }
  sendingQuoteMessage(imgPath: string): string {
    return `Sending QuoteImage: ${imgPath} ...`
  }
  quoteAnnouncement(): string {
    return 'The quote of today: '
  }

}
