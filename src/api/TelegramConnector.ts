import { randomUUID } from "crypto";
import { Telegraf } from "telegraf";
import { message } from 'telegraf/filters';
import { Logger } from "../Logger";
import { QuoteHandler } from "../QuoteHandler";
import { BotInitalized } from "../decorators/BotInitalized";
import { TextProvider } from "../textProvider/TextProvider";
import { Connector } from "./Connector";

export class TelegramConnector implements Connector {
  private readonly logger: Logger;
  private bot!: Telegraf
  private textProvider: TextProvider
  private quoteHandler: QuoteHandler

  constructor(logger: Logger, textProvider: TextProvider, quoteHandler: QuoteHandler) {
    this.logger = logger
    this.textProvider = textProvider
    this.quoteHandler = quoteHandler
  }

  async registerExternalClient(): Promise<void> {
    const bot = new Telegraf(process.env.TELEGRAM_BOT_TOKEN ?? '')

    bot.start((ctx) => ctx.reply('Welcome'));

    bot.telegram.setMyCommands(this.quoteHandler.getCommandExplanation())

    bot.launch()

    this.bot = bot

    process.once('SIGINT', () => this.bot?.stop('SIGINT'))
    process.once('SIGTERM', () => this.bot?.stop('SIGTERM'))
  }

  @BotInitalized()
  registerQuoteSubmitMethod(): void {

    this.bot.on(message('reply_to_message', 'text'), async (ctx) => {

      // @ts-expect-error is reply to text message (and no e.g. image)
      const quotedMessage = ctx.message.reply_to_message.text as string | undefined

      if (quotedMessage !== undefined && ctx.message.text.toLowerCase() === '/savequote') {
        const newQuoteId = randomUUID()
        const { error, savedQuoteId } = await this.quoteHandler.saveQuote({
          author: ctx.message.reply_to_message.from!.first_name,
          linkToOrigin: '' + ctx.message.reply_to_message.message_id,
          quote: quotedMessage,
          id: newQuoteId
        })

        if (error !== null || savedQuoteId === null) {
          ctx.reply(this.textProvider.unknownErrorMessage())
          throw error
        }

        ctx.reply(this.textProvider.quoteSavedMessage(savedQuoteId))
      } else if (ctx.message.text.startsWith('/')) {
        ctx.reply(this.textProvider.unknownCommandMessage(ctx.message.text))
      }
    })
  }

  @BotInitalized()
  registerRandomQuoteFetchingMethod(allowRandomQuoteFetching: boolean): void {
    if (!allowRandomQuoteFetching) {
      this.bot.command('getquote', async (ctx) => {

        ctx.reply(this.textProvider.fetchRandomQuoteDisallowedMessage())
      })
      return
    }

    this.bot.command('getquote', async (ctx) => {
      const imgPath = await this.quoteHandler.createRandomQuoteImage()
      this.postImage('' + ctx.chat.id, imgPath)
    })
  }

  @BotInitalized()
  async postImage(channelId: string, imgPath: string) {

    this.bot.telegram.sendPhoto(channelId, {
      source: imgPath
    })
  }
}
