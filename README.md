# QuoBot

A bot which can save & send quotes which supports multiple clients like Slack & Telegram

# Documentation ToDo
- create supabase type file
- external image URL

## Configuration

### Environement Variables

Slack
```sh
SLACK_SIGNING_SECRET="xxx"
SLACK_BOT_TOKEN="xxx"
SLACK_APP_TOKEN="xxx"
```

General
```sh
```

Supabase
```sh
SUPBABASE_URL=""
SUPABASE_ACCESS_KEY="xxx"
```

### Setup Supabase Persistance Client

- create a Table called `Quotes`
- create the following columns in the `Quotes` table:
  - id: int8
  - timestamp: timestampZ
  - author: text
  - quote: text
  - linkToOrigin: text
- active following RLS policies:
  - `Enable Read access to everyone`
- add sql function `get_random_quote`
  - ```
  create or replace function get_random_quote()
returns setof "Quotes"
language sql
as $$
   select * from "Quotes"
   order by random();
$$;
  ```



```SQL Enable Read access for everyone
CREATE POLICY "Enable read access for all users" ON "public"."Quotes"
AS PERMISSIVE FOR SELECT
TO public
USING (true)
```

```SQL Enable Read access for everyone
CREATE POLICY "Enable write access for all users" ON "public"."Quotes"
AS PERMISSIVE FOR INSERT
TO public
WITH CHECK (true)
```


## Used Resources

[JMH Typewriter](https://www.dafont.com/jmh-typewriter.font)
[Theatre Image](https://unsplash.com/de/fotos/WW1jsInXgwM)
