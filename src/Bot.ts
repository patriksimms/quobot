import { parseExpression as parseCronExpression } from 'cron-parser'
import { Cron } from 'croner'
import { ImageProvider } from './ImageProvider'
import { Logger } from './Logger'
import { QuoteHandler } from './QuoteHandler'
import { type Connector } from './api/Connector'
import { type PersistanceClient } from './persistence/PersistanceClient.interface'
import { TextProvider } from './textProvider/TextProvider'
import { EN } from './textProvider/EN'

interface Options {
  persistanceClient: PersistanceClient
  imageProvider?: ImageProvider
  textProvider?: TextProvider
  quoteHandler?: QuoteHandler
  connector: { new(logger: Logger, textProvider: TextProvider, quoteHandler: QuoteHandler): Connector }
}

export class Bot {
  private readonly persistanceClient: PersistanceClient
  private readonly imageProvider: ImageProvider
  private readonly connector: Connector
  private readonly logger: Logger
  private started: boolean = false
  private readonly textProvider: TextProvider
  private readonly quoteHandler: QuoteHandler

  constructor(options: Options, logger: Logger) {
    this.logger = logger
    this.persistanceClient = options.persistanceClient
    this.textProvider = options.textProvider ?? new EN()
    if (options.imageProvider) {
      this.imageProvider = options.imageProvider
    } else {
      this.imageProvider = new ImageProvider(logger)
    }
    if (process.env.EXTERNAL_IMAGE_URL !== undefined && process.env.EXTERNAL_IMAGE_URL !== '') {
      this.imageProvider.backgroundImage = process.env.EXTERNAL_IMAGE_URL
    }

    this.quoteHandler = options.quoteHandler ?? new QuoteHandler(this.persistanceClient, this.textProvider, this.imageProvider, this.logger)
    this.connector = new options.connector(
      this.logger,
      this.textProvider,
      this.quoteHandler
    )
  }

  async start(allowRandomQuoteFetching: boolean) {
    await this.connector.registerExternalClient()
    this.connector.registerQuoteSubmitMethod()
    this.connector.registerRandomQuoteFetchingMethod(allowRandomQuoteFetching)
    this.started = true
  }

  startPostQuoteCron(cronString?: string, quoteSendChannel?: string) {
    if (!this.started) {
      this.logger.error('Bot not started yet!')
      throw new Error('Bot not started yet!')
    }

    if (cronString === undefined || cronString === '') {
      this.logger.error(`provided cron string ${cronString} is invalid!`)
      throw new Error(`provided cron string ${cronString} is invalid!`)
    }

    if (quoteSendChannel === undefined || quoteSendChannel === '') {
      this.logger.error('quoteSendChannel is empty! Cannot send quotes automatically')
      throw new Error('quoteSendChannel is empty! Cannot send quotes automatically')
    }

    Cron(cronString, {
      timezone: 'Europe/Berlin'
    }, async () => {
      const quote = await this.persistanceClient.getRandomQuote()
      this.connector.postQuote(quoteSendChannel, quote, this.imageProvider)
    })

    const nextExecution = new Date(parseCronExpression(cronString).next().toISOString())
    this.logger.info(`cron ${cronString} started! The bot will send a quote at ${nextExecution.toLocaleString()}`)
  }
}
