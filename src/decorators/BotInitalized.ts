export const BotInitalized = () => {
  return function(_target: any, propertyKey: string, descriptor: PropertyDescriptor): PropertyDescriptor {
    const originalMethod = descriptor.value

    descriptor.value = function (this: any, ...args: any[]): any {
      this.logger.debug(`existance of bot is checked for ${propertyKey}...`)
      if (this.bot === undefined) {
        if (this.logger !== undefined && this.logger !== null) {
          this.logger.error('bot is not instantiated yet!')
        }
        throw new Error('bot is not instantiated yet!')
      }
      return originalMethod.apply(this, args)
    } 

    return descriptor
  }
}
